/*
  Assertions and other ways to crash.
*/
#ifndef _assert_hpp_include_
#define _assert_hpp_include_

#include "types.hpp"

[[noreturn]]
void panic(cstring msg);

void assert_expr(bool cond, cstring msg = nullptr);

void bounds_check(bool cond, cstring msg = nullptr);

#endif /* Include guard */
