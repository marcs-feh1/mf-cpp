/*
  Useful functions for manipulating memory.
*/
#ifndef _memory_hpp_include_
#define _memory_hpp_include_

#include "types.hpp"
#include "memory.hpp"

#include "info.hpp"

#if (COMPILER_VENDOR_CLANG) || (COMPILER_VENDOR_GCC)
	#define USE_BUILTIN_MEM
#else
	#include <cstring>
#endif

void* mem_set(void* dest, byte value, isize count){
#ifdef USE_BUILTIN_MEM
	__builtin_memset(dest, value, count);
#else
	memset(dest, value, count);
#endif
	return dest;
}

void* mem_copy_no_overlap(void * dest, void const * src, isize count){
#ifdef USE_BUILTIN_MEM
	__builtin_memcpy(dest, src, count);
#else
	memcpy(dest, src, count);
#endif
	return dest;
}

void* mem_copy(void * dest, void const * src, isize count){
#ifdef USE_BUILTIN_MEM
	__builtin_memmove(dest, src, count);
#else
	memmove(dest, src, count);
#endif
	return dest;
}


#endif /* Include guard */
