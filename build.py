from os import scandir

OUT = 'test'
BUILD_FILE = 'build.ninja'

ninja_header = '''
cxx = g++
cflags = -O1 -g -pipe -Wall -Wextra -I.
ldflags = -L.

rule compile
  command = $cxx $cflags -c $in -o $out

rule link
  command = $cxx $ldflags $in -o $out
'''

cxx_files = []

def extract_quoted(inc: str) -> tuple[str, bool]:
    first = inc.find('"')
    second = inc.find('"', first + 1)

    if first < 0 or second < 0:
        return '', False

    return inc[first+1:second], True

with scandir('.') as iter:
    for entry in iter:
        if entry.is_file and entry.name.endswith('.cpp'):
            cxx_files.append({'file': entry.name, 'includes': []})
            with open(entry.name, 'r') as entry:
                lines = entry.readlines()
                for line in map(lambda x: x.strip(), lines):
                    if line.startswith('#include'):
                        included, ok = extract_quoted(line)
                        if ok: cxx_files[-1]['includes'].append(included)

with open(BUILD_FILE, 'w') as f:
    written_bytes = 0
    written_bytes += f.write(ninja_header.strip() + '\n\n')
    for entry in cxx_files:
       written_bytes +=  f.write(f'build {entry["file"]}.o: compile {entry["file"]} | {" ".join(entry["includes"])}\n')

    written_bytes += f.write(f'build {OUT}: link ' + ' '.join(f'{e["file"]}.o' for e in cxx_files))
    written_bytes += f.write('\n\n')

    print(f'Wrote {written_bytes}B to {BUILD_FILE}')

