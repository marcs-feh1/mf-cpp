/*
  Essential types.
*/
#ifndef _types_hpp_include_
#define _types_hpp_include_

#include <cstdint>
#include <cstddef>

using i8  = int8_t;
using i16 = int16_t;
using i32 = int32_t;
using i64 = int64_t;
using u8  = uint8_t;
using u16 = uint16_t;
using u32 = uint32_t;
using u64 = uint64_t;

using byte = u8;
using rune = i32;
using cstring = char const * const;

using uintptr = uintptr_t;
using isize = ptrdiff_t;
using usize = size_t;

using f32 = float;
using f64 = double;


static_assert(sizeof(isize) == sizeof(usize), "Mismatched size types.");
static_assert(sizeof(f32) == 4, "Unsupported float.");
static_assert(sizeof(f64) == 8, "Unsupported float.");

#endif /* Include guard */
