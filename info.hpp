/*
  Information about the current environment.
*/
#ifndef _info_hpp_include_
#define _info_hpp_include_

// NOTE: Do **NOT** change the order of the ifdefs
#if defined(__clang__)
	#define COMPILER_VENDOR_CLANG 1
#elif defined(__GNUC__) && !defined(__clang__)
	#define COMPILER_VENDOR_GCC 1
#elif defined(_MSVC_VER)
	#define COMPILER_VENDOR_MSVC 1
#else
	#define COMPILER_VENDOR_UNKNOWN 1
#endif

#endif /* Include guard */
