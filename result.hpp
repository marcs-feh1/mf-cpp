/*
  Tagged union between an expected data type and an error type (usually an enum).
  Note that the "error type" is assumed to be default constructible.
*/
#ifndef _result_hpp_include_
#define _result_hpp_include_

// NOTE: Don't you just *LOVE* boilerplate?
#include "assert.hpp"
#include "utility.hpp"
#include <new>

template<typename Ok, typename Err>
struct Result {
private:
	union {
		Err error;
		Ok data;
	};
	bool has_value;

public:
	constexpr
	bool ok() const {
		return has_value;
	}

	constexpr
	Result() : has_value{false} {}

	constexpr
	Result(Result const& res)
		: has_value(res.has_value)
	{
		if(has_value){
			reset(res.data);
		} else {
			reset(res.error);
		}
	}

	constexpr
	Result(Result && res)
		: has_value(u::exchange(res.has_value, false))
	{
		if(has_value){
			reset(u::move(res.data));
		} else {
			reset(u::move(res.error));
		}
	}

	// Unconditionally destroy whatever is inside the result
	constexpr
	void destroy(){
		if(has_value){
			data.~Ok();
		}
		else {
			error.~Err();
		}
		has_value = false;
	}

	// Destroy whatever is in Result, then set a new value.
	constexpr
	void reset(Ok const& val){
		destroy();
		new (&data) Ok(val);
		has_value = true;
	}
	constexpr
	void reset(Ok&& val){
		destroy();
		new (&data) Ok(u::move(val));
		has_value = true;
	}

	constexpr
	void reset(Err const& val){
		destroy();
		new (&data) Err(val);
		has_value = false;
	}
	constexpr
	void reset(Err&& val){
		destroy();
		new (&data) Err(u::move(val));
		has_value = false;
	}

	constexpr
	Ok get() const& {
		assert_expr(has_value, "Cannot get() value from error result.");
		return data;
	}

	constexpr
	Ok get() && {
		assert_expr(has_value, "Cannot get() value from error result.");
		Ok tmp = u::move(data);
		has_value = false;
		return tmp;
	}

	constexpr
	Err get_error() const& {
		assert_expr(!has_value, "Cannot get_error() from ok result.");
		return error;
	}

	constexpr
	Err get_error() && {
		assert_expr(!has_value, "Cannot get_error() from ok result.");
		Err tmp = u::move(error);
		return tmp;
	}

	template<typename U>
	constexpr
	Ok get_or(U&& alt) const& {
		if(has_value){
			return data;
		}
		return alt;
	}

	template<typename U>
	constexpr
	Ok get_or(U&& alt) && {
		if(has_value){
			auto val = u::move(*this).get();
			return val;
		}
		return u::forward<U>(alt);
	}

	~Result(){
		destroy();
	}
};

template<typename Ok, typename Err>
Result<Ok, Err> make_result(Ok&& val){
	Result<Ok, Err> res;
	res.reset(u::forward<Ok>(val));
	return res;
}

template<typename Ok, typename Err>
Result<Ok, Err> make_result(Err&& val){
	Result<Ok, Err> res;
	res.reset(u::forward<Err>(val));
	return res;
}
#endif /* Include guard */
