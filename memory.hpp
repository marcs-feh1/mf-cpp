#ifndef _memory_hpp_include_
#define _memory_hpp_include_

#include "types.hpp"

void* mem_copy(void * dest, void const * src, isize count);

void* mem_copy_no_overlap(void * dest, void const * src, isize count);

void* mem_set(void* dest, byte value, isize count);

template<typename T>
constexpr
T align_forward(T value, T align){
	T mod = value % align;

	if(mod > 0){
		value += (align - mod);
	}

	return value;
}
#endif /* Include guard */
