#ifndef _io_hpp_include_
#define _io_hpp_include_

#include "slice.hpp"

namespace io {
enum class Error {
	None = 0,

	EndOfFile,
	NotEnoughSize,
	ClosedSource,
	ClosedDest,

	/* Generic errors. */
	ReadFailure,
	WriteFailure,
};

/* IO Reader interface */
struct Reader {
	// Reads into buf, returns number of read bytes
	virtual isize read(slice<byte> buf) = 0;
};

/* IO Writer interface */
struct Writer {
	// Writes from buf, returns number of bytes written
	virtual isize write(slice<byte> buf) = 0;
};

/* IO Reader|Writer */
struct ReaderWriter : public Reader, public Writer {};

}

#endif /* Include guard */
