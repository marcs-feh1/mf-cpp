/*
  General utilities, a less bloated version of the standard <utility> header.
*/

#ifndef _utility_hpp_include_
#define _utility_hpp_include_

#include "types.hpp"

namespace u {
// Absolute value
template<typename T>
constexpr
T abs(T const& x){
	if(x < 0){
		return -x;
	}
	return x;
}

// Maximum
template<typename T>
constexpr
T max(T const& a, T const& b){
	if(b > a){ return b; }
	return a;
}

// Minimum
template<typename T>
constexpr
T min(T const& a, T const& b){
	if(b < a){ return b; }
	return a;
}

// Maximum
template<typename T, typename ...Rest>
constexpr
T max(T const& a, T const& b, Rest&& ...rest){
	if(b > a){
		return max(b, rest...);
	}
	return max(a, rest...);
}

// Minimum
template<typename T, typename ...Rest>
constexpr
T min(T const& a, T const& b, Rest&& ...rest){
	if(b < a){
		return min(b, rest...);
	}
	return min(a, rest...);
}

// Make x fit the rage mini..maxi (inclusive)
template<typename T>
constexpr
T clamp(T const& mini, T const& x, T const& maxi){
	return min(max(x, mini), maxi);
}

namespace typing {
template<typename T>
struct remove_reference_type {typedef T type; };
template<typename T>
struct remove_reference_type<T&> {typedef T type; };
template<typename T>
struct remove_reference_type<T&&> {typedef T type; };

template<typename T, T v>
struct integral_constant {
	static constexpr T value = v;
	typedef T value_type;
	constexpr operator value_type() { return value; }
};

using true_type  = integral_constant<bool, true>;
using false_type = integral_constant<bool, false>;

template<typename A, typename B>
struct same_type : false_type {};

template<typename T>
struct same_type<T, T> : true_type {};

template<typename A, typename B>
constexpr auto same_as = same_type<A, B>::value;

template<typename T>
struct is_lvalue_reference_type : false_type {};
template<typename T>
struct is_lvalue_reference_type<T&> : true_type {};
template<typename T>
struct is_lvalue_reference_type<T&&> : false_type {};

template<typename T>
struct is_rvalue_reference_type : false_type {};
template<typename T>
struct is_rvalue_reference_type<T&> : false_type {};
template<typename T>
struct is_rvalue_reference_type<T&&> : true_type {};

template<typename T>
using remove_reference = typename remove_reference_type<T>::type;

template<typename T>
constexpr bool is_lvalue_ref = is_lvalue_reference_type<T>::value;

template<typename T>
constexpr bool is_rvalue_ref = is_rvalue_reference_type<T>::value;
}

// Cast x to rvalue reference
template<typename T>
constexpr
typing::remove_reference<T>&& move(T&& x) noexcept {
	using rv = typing::remove_reference<T>&&;
	return static_cast<rv>(x);
}

// Contitionally moves x, if and only if, x is an rvalue reference.
// Requires passing template type explicitly. This is used to implement
// "perfect forwarding"
template<typename T>
constexpr
T&& forward(typing::remove_reference<T>& x) noexcept {
	return static_cast<T&&>(x);
}

// Contitionally moves x, if and only if, x is an rvalue reference.
// Requires passing template type explicitly. This is used to implement
// "perfect forwarding"
template<typename T>
constexpr
T&& forward(typing::remove_reference<T>&& x) noexcept {
	static_assert(
		!typing::is_lvalue_ref<T>,
		"Cannot use forward() to convert an rvalue to an lvalue"
	);
	return static_cast<T&&>(x);
}

// Swap values of a and b
template<typename T>
constexpr
void swap(T& a, T& b) noexcept {
	T t = u::move(b);
	b   = u::move(a);
	a   = u::move(t);
}

// Replaces x with val and returns the old value of x
template<typename T, typename U = T>
[[nodiscard]] constexpr
T exchange(T& x, U&& val) noexcept {
	T t = u::move(x);
	x   = u::forward<U>(val);
	return t;
}

template<typename A, typename B = A>
struct pair {
	A a;
	B b;
};

// Check if an error enum is zero.
// If it is a boolean, check if it's true (ok)
template<typename EnumType>
constexpr
bool error_ok(EnumType e){
	return isize(e) == 0;
}

template<>
constexpr
bool error_ok<bool>(bool e){
	return e;
}

template<typename T, typename ... Args>
constexpr
void construct(T* ptr, Args&& ... args){
	new (ptr) T(u::forward<Args>(args)...);
}

template<typename T, typename ... Args>
constexpr
void destruct(T& val){
	val.~T();
}
}

#endif /* Include guard */
