#include "assert.hpp"

#include <cstdlib>
#include <cstdio>

#define HALT_AND_CATCH_FIRE() std::abort()
void panic(cstring msg){
	std::fprintf(stderr, "Panic: %s\n", msg);
	HALT_AND_CATCH_FIRE();
}


void assert_expr(bool cond, cstring msg){
#ifndef DISABLE_ASSERT
	[[unlikely]] if(!cond){
		if(msg != nullptr){
			std::fprintf(stderr, "Assertion failure: %s\n", msg);
		}
		else {
			std::fprintf(stderr, "Assertion failure.\n");
		}
		HALT_AND_CATCH_FIRE();
	}
#else
	(void)cond; (void)msg;
#endif
}

void bounds_check(bool cond, cstring msg){
#ifndef DISABLE_BOUNDS_CHECK
	[[unlikely]] if(!cond){
		if(msg != nullptr){
			std::fprintf(stderr, "Bounds check failure: %s\n", msg);
		}
		else {
			std::fprintf(stderr, "Bounds check failure.\n");
		}
		HALT_AND_CATCH_FIRE();
	}
#else
	(void)cond; (void)msg;
#endif
}

#undef HALT_AND_CATCH_FIRE
