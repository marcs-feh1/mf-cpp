#include "assert.hpp"
#include "slice.hpp"
#include "result.hpp"
#include <memory>
#include <iostream>

enum class Error {
	Oops,
};

auto alloc_unique(bool b){
	if(b){
		auto x = std::make_unique<int>(69);
		return make_result<std::unique_ptr<int>, int>(u::move(x));
	}
	return make_result<std::unique_ptr<int>, int>(69);
}

int main(){
	auto a = alloc_unique(false).get_or(std::make_unique<int>(100));
	std::cout << *a << '\n';
}
