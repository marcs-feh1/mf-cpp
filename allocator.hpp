/*
  Memory allocator interface.
*/
#ifndef _allocator_hpp_include_
#define _allocator_hpp_include_

#include "memory.hpp"
#include "result.hpp"
#include "slice.hpp"

// Allocator interface
struct Allocator {
	enum Error : i8 {
		None = 0,
		OutOfMemory,
		BadSize,
		BadAlignment,
		UnsupportedOperation,
	};

	// Allocate a new block of memory of a size with a particular alignment
	virtual Result<void*, Error> alloc(isize size, isize alignment) = 0;

	// Try to resize a block in-place to be new_size bytes long
	virtual Result<void*, Error> resize(void* old_ptr, isize old_size, isize new_size, isize alignment) = 0;

	// Free a pointer
	virtual Error free(void* ptr) = 0;

	// Free all pointers supported by allocator
	virtual Error free_all() = 0;
};

using AllocatorError = Allocator::Error;

template<typename T>
T* create(Allocator& allocator){
	auto p = allocator.alloc(sizeof(T), alignof(T)).get();
	auto obj = static_cast<T*>(mem_set(p, 0, sizeof(T)));
	new (&obj) T();
	return obj;
}

template<typename T, typename ... Args>
T* create(Allocator& allocator, Args&& ... args){
	auto p = allocator.alloc(sizeof(T), alignof(T)).get();
	auto obj = static_cast<T*>(mem_set(p, 0, sizeof(T)));
	new (&obj) T(u::forward<Args>(args)...);
	return obj;
}

#endif /* Include guard */
