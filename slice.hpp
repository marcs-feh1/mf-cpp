/*
  A slice what C arrays should have been from the start, a sized view into
  contigous memory.
*/
#ifndef _slice_hpp_include_
#define _slice_hpp_include_

#include "types.hpp"
#include "assert.hpp"

template<typename T>
struct slice {
private:
	T* data = nullptr;
	isize length = 0;

public:
	constexpr
	T& operator[](isize idx){
		bounds_check(idx >= 0 && idx < length, "Index out of bounds");
		return data[idx];
	}

	constexpr
	T const& operator[](isize idx) const {
		bounds_check(idx >= 0 && idx < length, "Index out of bounds");
		return data[idx];
	}

	constexpr
	T* raw_data() const {
		return data;
	}

	constexpr
	isize size() const {
		return length;
	}

	constexpr
	bool empty() const {
		return length == 0 || data == nullptr;
	}

	static constexpr
	slice from(T* ptr, isize length){
		assert_expr(length >= 0, "Negative length slice is not allowed");
		auto s = slice<T>();
		s.data = ptr;
		s.length = length;
		return s;
	}

	slice(){}
};

#endif /* Include guard */
